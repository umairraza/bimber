<?php
/**
 * The Template for displaying the home page.
 *
 * @license For the full license information, please view the Licensing folder
 * that was distributed with this source code.
 *
 * @package Bimber_Theme
 */

// Prevent direct script access.
if ( ! defined( 'ABSPATH' ) ) {
	die( 'No direct script access allowed' );
}

get_header();
?>

<?php
$bimber_home_settings = bimber_get_home_settings();
bimber_set_template_part_data( $bimber_home_settings );
?>

<?php
if ( bimber_show_home_featured_entries() ) :
	get_template_part( 'template-parts/featured-content-3' );
endif;
?>

	<div class="g1-row g1-row-layout-page g1-row-padding-m archive-body">
		<div class="g1-row-inner">

			<div id="primary" class="g1-column g1-column-2of3">

				<?php get_template_part( 'template-parts/collection/title', 'home' ); ?>

				<?php if ( have_posts() ) : ?>
					<div class="g1-collection">
						<div class="g1-collection-viewport">
							<ul class="g1-collection-items">
								<?php $bimber_post_number = 0; ?>
								<?php while ( have_posts() ) : the_post();
									$bimber_post_number ++; ?>
									<?php do_action( 'bimber_home_loop_before_post', 'classic', $bimber_post_number ); ?>

									<li class="g1-collection-item">
										<?php get_template_part( 'template-parts/content', get_post_format() ); ?>
									</li>

									<?php do_action( 'bimber_home_loop_after_post', 'classic', $bimber_post_number ); ?>
								<?php endwhile; ?>
							</ul>
						</div>

						<?php get_template_part( 'template-parts/archive-pagination', $bimber_home_settings['pagination'] ); ?>
					</div>
				<?php else : ?>
					<?php get_template_part( 'template-parts/archive-no-results' ); ?>
				<?php endif; ?>

			</div><!-- .g1-column -->

			<?php get_sidebar(); ?>

		</div>
		<div class="g1-row-background"></div>
	</div>

<?php bimber_reset_template_part_data(); ?>

<?php get_footer();
