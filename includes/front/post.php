<?php
/**
 * Post functions
 *
 * @license For the full license information, please view the Licensing folder
 * that was distributed with this source code.
 *
 * @package Bimber_Theme
 */

// Prevent direct script access.
if ( ! defined( 'ABSPATH' ) ) {
	die( 'No direct script access allowed' );
}

/**
 * Alters single post template based on theme options
 *
 * @param  string $template Template.
 *
 * @return string
 */
function bimber_post_alter_single_template( $template ) {
	$object = get_queried_object();

	if ( 'post' !== $object->post_type ) {
		return $template;
	}

	$single_post_options = get_post_meta( $object->ID, '_bimber_single_options', true );

	if ( ! empty( $single_post_options['template'] ) ) {
		$post_template = $single_post_options['template'];
	} else {
		$post_template = bimber_get_theme_option( 'post', 'template' );
	}

	$filename = sprintf( 'g1-template-post-%s', $post_template );

	$templates = array();

	// Keep in mind the WordPress template hierarchy
	// Read more about it here https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post .
	array_unshift( $templates,
		"{$filename}-{$object->post_type}-{$object->post_name}.php",
		"{$filename}-{$object->post_type}.php",
		"{$filename}.php"
	);

	$templates = array_unique( $templates );

	if ( count( $templates ) ) {
		$new_template = locate_template( $templates );

		if ( ! empty( $new_template ) ) {
			return $new_template;
		}
	}

	return $template;
}

/**
 * Adjust post classes.
 *
 * @param array $classes Post classes.
 *
 * @return array
 */
function bimber_post_class( $classes ) {
	// Remove classes.
	return array_diff( $classes, array(
		// We'll be using schema.org microdata instead of microformats.
		'hentry'
	) );
}

/**
 * Get default settings for a post
 *
 * @return mixed|void
 */
function bimber_get_post_default_settings() {
	return apply_filters( 'bimber_post_default_settings', array(
		'template' => 'classic',
		'elements' => array(
			'featured_media'  => true,
			'categories'      => true,
			'author'          => true,
			'avatar'          => true,
			'date'            => true,
			'comments_link'   => true,
			'shares_top'      => true,
			'tags'            => true,
			'shares_bottom'   => true,
			'newsletter'      => true,
			'navigation'      => true,
			'author_info'     => true,
			'related_entries' => true,
			'more_from'       => true,
			'dont_miss'       => true,
			'comments'        => true,
			'views'           => true,
		),
	) );
}

/**
 * Get post settings
 *
 * @return mixed|void
 */
function bimber_get_post_settings() {
	return apply_filters( 'bimber_post_settings', array(
		'template' => bimber_get_theme_option( 'post', 'template' ),
		'elements' => bimber_get_post_elements_visibility_arr( bimber_get_theme_option( 'post', 'hide_elements' ) ),
	) );
}

/**
 * Get the post elements visibility configuration
 *
 * @param string $elements_to_hide_str Comma-separated list of elements to hide.
 *
 * @return mixed
 */
function bimber_get_post_elements_visibility_arr( $elements_to_hide_str ) {
	$elements_to_hide_arr = explode( ',', $elements_to_hide_str );
	$defaults             = bimber_get_post_default_settings();
	$all_elements         = $defaults['elements'];

	foreach ( $all_elements as $elem_id => $is_visible ) {
		if ( in_array( $elem_id, $elements_to_hide_arr, true ) ) {
			$all_elements[ $elem_id ] = false;
		}
	}

	return $all_elements;
}

/**
 * Get ids of related posts
 *
 * @param int $post_id Post id.
 * @param int $limit Maximum number of ids to return.
 * @param int $min_entries Minimum number of ids to return.
 *
 * @return array
 */
function bimber_get_related_posts_ids( $post_id = 0, $limit = 10, $min_entries = 0 ) {
	return bimber_get_related_entries_ids( $post_id, 'post', $limit, $min_entries );
}

/**
 * Get ids of related entries
 *
 * @param int    $post_id Post id.
 * @param string $post_type Post type.
 * @param int    $limit Limit.
 * @param int    $min_entries Minimum entries.
 *
 * @return array
 */
function bimber_get_related_entries_ids( $post_id = 0, $post_type = 'post', $limit = 10, $min_entries = 0 ) {
	if ( ! $post_id ) {
		global $post;

		$post_id = $post ? $post->ID : 0;
	}

	$min_entries = min( $min_entries, $limit );

	$post_id = absint( $post_id );

	if ( $post_id <= 0 ) {
		return array();
	}

	$related_ids = array();

	$tags = get_the_terms( $post_id, 'post_tag' );

	if ( ! empty( $tags ) ) {
		$tag_ids = wp_list_pluck( $tags, 'term_id' );

		global $wpdb;

		$tag_ids = implode( ', ', array_map( 'intval', $tag_ids ) );

		// Custom SQL query.
		// Standard query_posts function doesn't have enough power to produce results we need.
		$bimber_query = $wpdb->prepare(
			"
				SELECT p.ID, COUNT(t_r.object_id) AS cnt
	            FROM {$wpdb->term_relationships} AS t_r, {$wpdb->posts} AS p
	            WHERE t_r.object_id = p.ID
	                AND t_r.term_taxonomy_id IN( $tag_ids )
	                AND p.post_type= %s
	                AND p.ID != %d
	                AND p.post_status= %s
	            GROUP BY t_r.object_id
	            ORDER BY cnt DESC, p.post_date_gmt DESC
			",
			$post_type,
			$post_id,
			'publish'
		);

		if ( $limit > 0 ) {
			$bimber_query .= $wpdb->prepare( ' LIMIT %d', $limit );
		}

		// Run the query.
		$posts = $wpdb->get_results( $bimber_query );

		if ( ! empty( $posts ) ) {
			foreach ( $posts as $p ) {
				$related_ids[] = (int) $p->ID;
			}
		}
	}

	// Complement entries.
	if ( $min_entries > 0 && count( $related_ids ) < $min_entries ) {
		$entires_to_add = $min_entries - count( $related_ids );

		$query_args = array(
			'posts_per_page'        => $entires_to_add,
			'post_type'             => $post_type,
			'post_status'           => 'publish',
			'post__not_in'          => array_merge( $related_ids, array( $post_id ) ),
			'ignore_sticky_posts'   => true,
		);

		$query = new WP_Query();
		$posts = $query->query( $query_args );

		foreach ( $posts as $post ) {
			$related_ids[] = $post->ID;
		}
	}

	return $related_ids;
}

/**
 * Get post taxonomies
 *
 * @param int  $post_id Post id.
 * @param bool $hierarchical Whether or not to return hierarchical taxonomies.
 *
 * @return mixed|void
 */
function bimber_get_post_taxonomies( $post_id, $hierarchical = true ) {
	$post_obj           = get_post( $post_id );
	$taxonomy_objects   = get_object_taxonomies( $post_obj, 'objects' );

	// Remove taxonomies.
	foreach ( $taxonomy_objects as $name => $object ) {
		// Non-public.
		if ( ! $object->query_var ) {
			unset( $taxonomy_objects[ $name ] );
		}

		// None hierarchical, if hierarchical requested.
		if ( $hierarchical && ! $object->hierarchical ) {
			unset( $taxonomy_objects[ $name ] );
		}

		// Hierarchical, if none hierarchical requested.
		if ( ! $hierarchical && $object->hierarchical ) {
			unset( $taxonomy_objects[ $name ] );
		}
	}

	return apply_filters( 'bimber_post_taxonomies', $taxonomy_objects );
}

/**
 * Get post terms
 *
 * @param int  $post_id Post id.
 * @param bool $hierarchical_taxonomies Whether or not include hierarchical terms.
 *
 * @return array
 */
function bimber_get_post_terms( $post_id, $hierarchical_taxonomies = true ) {
	$taxonomies = bimber_get_post_taxonomies( $post_id, $hierarchical_taxonomies );

	$taxonomy_terms = array();

	foreach ( $taxonomies as $object ) {
		$terms = apply_filters( 'bimber_post_terms', get_the_terms( $post_id, $object->name ) );

		if ( ! empty( $terms ) ) {
			$taxonomy_terms[ $object->name ] = $terms;
		}
	}

	return $taxonomy_terms;
}

/**
 * Get the first category assigned to post
 *
 * @param int $post_id Post id.
 *
 * @return mixed|null
 */
function bimber_get_post_first_category( $post_id ) {
	$terms = bimber_get_post_terms( $post_id, true );

	if ( empty( $terms ) ) {
		return null;
	}

	$first_taxonomy_terms = array_shift( $terms );
	$first_term           = array_shift( $first_taxonomy_terms );

	return $first_term;
}

/**
 * Whether a post is popular.
 *
 * @param int|WP_Post $p Optional. Post ID or WP_Post object. Default is global `$post`.
 *
 * @return bool
 */
function bimber_is_post_popular( $p = null ) {
	$post_obj = get_post( $p );

	$meta_value = get_post_meta( $post_obj->ID, '_bimber_popular', true );

	return apply_filters( 'bimber_is_post_popular', ! empty( $meta_value ), $post_obj->ID );
}

/**
 * Get ids of popular posts
 *
 * @param int $limit Maximum number of ids to return.
 *
 * @return array
 */
function bimber_get_popular_post_ids( $limit = 10 ) {
	$ids = array();

	$query_args = array(
		'meta_key'            => '_bimber_popular',
		'orderby'             => 'meta_value_num',
		'order'               => 'ASC',
		'posts_per_page'      => $limit,
		'ignore_sticky_posts' => true,
	);

	$query = new WP_Query();
	$posts = $query->query( $query_args );

	foreach ( $posts as $post ) {
		$ids[] = $post->ID;
	}

	return apply_filters( 'bimber_popular_post_ids', $ids, $limit );
}

/**
 * Whether the post is hot.
 *
 * @param int|WP_Post $p Optional. Post ID or WP_Post object. Default is global `$post`.
 *
 * @return bool
 */
function bimber_is_post_hot( $p = null ) {
	$post_obj = get_post( $p );

	$meta_value = get_post_meta( $post_obj->ID, '_bimber_hot', true );

	return apply_filters( 'bimber_is_post_hot', ! empty( $meta_value ), $post_obj->ID );
}

/**
 * Get ids of hot posts
 *
 * @param int $limit Maximum number of ids to return.
 *
 * @return array
 */
function bimber_get_hot_post_ids( $limit = 10 ) {
	$ids = array();

	$query_args = array(
		'meta_key'            => '_bimber_hot',
		'orderby'             => 'meta_value_num',
		'order'               => 'ASC',
		'posts_per_page'      => $limit,
		'ignore_sticky_posts' => true,
	);

	$query = new WP_Query();
	$posts = $query->query( $query_args );

	foreach ( $posts as $post ) {
		$ids[] = $post->ID;
	}

	return apply_filters( 'bimber_hot_post_ids', $ids, $limit );
}

/**
 * Whether the post is trending.
 *
 * @param int|WP_Post $p Optional. Post ID or WP_Post object. Default is global `$post`.
 *
 * @return bool
 */
function bimber_is_post_trending( $p = null ) {
	$post_obj = get_post( $p );

	$meta_value = get_post_meta( $post_obj->ID, '_bimber_trending', true );

	return apply_filters( 'bimber_is_post_trending', ! empty( $meta_value ), $post_obj->ID );
}

/**
 * Get ids of trending posts
 *
 * @param int $limit Maximum numbef of ids to return.
 *
 * @return mixed|void
 */
function bimber_get_trending_post_ids( $limit = 10 ) {
	$ids = array();

	$query_args = array(
		'meta_key'            => '_bimber_trending',
		'orderby'             => 'meta_value_num',
		'order'               => 'ASC',
		'posts_per_page'      => $limit,
		'ignore_sticky_posts' => true,
	);

	$query = new WP_Query();
	$posts = $query->query( $query_args );

	foreach ( $posts as $post ) {
		$ids[] = $post->ID;
	}

	return apply_filters( 'bimber_trending_post_ids', $ids, $limit );
}

/**
 * Check whether the Popular collection is enabled
 *
 * @return bool
 */
function bimber_has_popular_collection() {
	return (bool) bimber_get_theme_option( 'posts', 'popular_enable' );
}

/**
 * Check whether the Hot collection is enabled
 *
 * @return bool
 */
function bimber_has_hot_collection() {
	return (bool) bimber_get_theme_option( 'posts', 'hot_enable' );
}

/**
 * Check whether the Trending collection is enabled
 *
 * @return bool
 */
function bimber_has_trending_collection() {
	return (bool) bimber_get_theme_option( 'posts', 'trending_enable' );
}


/**
 * Get ids of featured posts
 *
 * @param array $query_args Query arguments.
 *
 * @return array
 */
function bimber_get_featured_posts_ids( $query_args ) {
	// Static var as a simple cache
	// in one request, it's enough to calculate featured ids just once.
	static $featured_ids;

	if ( isset( $featured_ids ) ) {
		return $featured_ids;
	}

	// WP_Query args.
	$defaults = array(

		'posts_per_page'      => 10,
		'post_type'           => 'post',
		'post_status'         => 'publish',
		'ignore_sticky_posts' => true,
		'category__in'        => array(),
		'tag__in'             => array(),
		// Custom args.
		'type'                => 'recent',
		'time_range'          => 'all',
	);

	$query_args = wp_parse_args( $query_args, $defaults );

	if ( bimber_show_global_featured_entries() && bimber_global_featured_entries_exclude_from_main_loop() ) {
		$global_featured_ids = bimber_get_global_featured_posts_ids();

		if ( ! empty( $global_featured_ids ) ) {
			$query_args['post__not_in'] = $global_featured_ids;
		}
	}

	// Remove custom args form $args.
	$type       = $query_args['type'];
	$time_range = $query_args['time_range'];

	unset( $query_args['type'] );
	unset( $query_args['time_range'] );

	// Map custom args to WP_Query args.
	$query_args = bimber_time_range_to_date_query( $time_range, $query_args );

	if ( is_category() ) {
		$query_args['category__in'][] = get_queried_object()->term_id;
	}

	if ( is_tag() ) {
		$query_args['tag__in'][] = get_queried_object()->term_id;
	}

	if ( is_tax() ) {
		$taxonomy = get_queried_object()->taxonomy;
		$term_id  = get_queried_object()->term_id;

		$query_args['tax_query'] = array(
			array(
				'taxonomy' => $taxonomy,
				'field'    => 'term_id',
				'terms'    => $term_id,
			),
		);
	}

	// Filter by author.
	if ( is_author() ) {
		$author = get_user_by( 'id', get_query_var( 'author' ) );

		// Try to get author by slug if ID not set.
		if ( false === $author ) {
			$author = get_user_by( 'slug', get_query_var( 'author_name' ) );
		}

		if ( false !== $author ) {
			$query_args['author'] = $author->ID;
		}
	}

	switch ( $type ) {
		case 'recent':
			$query_args['orderby'] = 'date';
			break;

		case 'most_shared':
			$query_args = bimber_get_most_shared_query_args( $query_args );
			break;

		case 'most_viewed':
			$query_args = bimber_get_most_viewed_query_args( $query_args, 'featured_posts_ids' );
			break;
	}

	$query_args = apply_filters( 'bimber_featured_posts_query_args', $query_args );
	$query = new WP_Query();
	$posts = $query->query( $query_args );

	$featured_ids = array();

	foreach ( $posts as $post ) {
		$featured_ids[] = $post->ID;
	}

	return $featured_ids;
}

/**
 * Format a number to a more compact form
 *
 * @param int $number Number.
 *
 * @return string
 */
function bimber_format_number( $number ) {
	$number_formatted = $number;

	if ( $number > 1000000 ) {
		$number_formatted = round( $number / 1000000, 1 ) . esc_html_x( 'M', 'formatted number suffix', 'bimber' );
	} elseif ( $number > 1000 ) {
		$number_formatted = round( $number / 1000, 1 ) . esc_html_x( 'k', 'formatted number suffix', 'bimber' );
	}

	return $number_formatted;
}

/**
 * Return query object for global featured entries
 *
 * @return WP_Query
 */
function bimber_get_global_featured_entries_query() {
	// Get built query from cache.
	$bimber_query = get_transient( 'bimber_featured_entries_query' );

	// Build cache if not set.
	if ( false === $bimber_query ) {
		$bimber_template 	= bimber_get_theme_option( 'featured_entries', 'template' );
		$bimber_type 		= bimber_get_theme_option( 'featured_entries', 'type' );
		$bimber_time_range 	= bimber_get_theme_option( 'featured_entries', 'time_range' );

		// Common args.
		$bimber_query_args = array(
			'posts_per_page'      => 'list' === $bimber_template ? 3 : 6,
			'ignore_sticky_posts' => true,
		);

		// Category.
		$bimber_query_args['category_name'] = bimber_get_theme_option( 'featured_entries', 'category' );

		if ( is_array( $bimber_query_args['category_name'] ) ) {
			$bimber_query_args['category_name'] = implode( ',', $bimber_query_args['category_name'] );
		}

		// Tag.
		$bimber_tags = array_filter( bimber_get_theme_option( 'featured_entries', 'tag' ) ); // array_filter removes empty values.

		if ( ! empty( $bimber_tags ) ) {
			$bimber_query_args['tag_slug__in'] = $bimber_tags;
		}

		// Time range.
		$bimber_query_args = bimber_time_range_to_date_query( $bimber_time_range, $bimber_query_args );

		// Type.
		switch ( $bimber_type ) {
			case 'recent':
				$bimber_query_args['orderby'] = 'post_date';
				break;

			case 'most_viewed':
				$bimber_query_args = bimber_get_most_viewed_query_args( $bimber_query_args, 'featured' );
				break;

			case 'most_shared':
				$bimber_query_args = bimber_get_most_shared_query_args( $bimber_query_args );
				break;
		}

		$bimber_query_args = apply_filters( 'bimber_global_featured_entries_query_args', $bimber_query_args );

		$bimber_query = new WP_Query( $bimber_query_args );

		set_transient( 'bimber_featured_entries_query', $bimber_query );
	}

	return $bimber_query;
}

/**
 * Return global featured posts ids
 *
 * @return array
 */
function bimber_get_global_featured_posts_ids() {
	$ids = array();

	if ( 'none' === bimber_get_theme_option( 'featured_entries', 'type' ) ) {
		return $ids;
	}

	$query = bimber_get_global_featured_entries_query();

	if ( $query->have_posts() ) {
		$posts = $query->get_posts();

		foreach ( $posts as $post ) {
			$ids[] = $post->ID;
		}
	}

	return $ids;
}

/**
 * Checks whether the post is NSFW
 *
 * @return bool
 */
function bimber_is_nsfw() {
	$bool = false;

	if ( bimber_get_theme_option( 'nsfw', 'enabled' ) ) {
		$nsfw_categories = bimber_get_nsfw_categories();

		if ( ! empty( $nsfw_categories ) && has_category( $nsfw_categories ) ) {
			$bool = true;
		}
	}

	return apply_filters( 'bimber_is_nsfw', $bool );
}

/**
 * Return ids of categories for NSFW posts.
 *
 * @return array		Array of ids.
 */
function bimber_get_nsfw_categories() {
	$ids = array();
	$slugs = explode( ',', bimber_get_theme_option( 'nsfw', 'categories_ids' ) );

	foreach ( $slugs as $slug ) {
		$category = get_category_by_slug( $slug );

		if ( $category ) {
			$ids[] = $category->term_id;
		}
	}

	return $ids;
}

/**
 * Returns media ids for the first [gallery] shortcode in post content
 *
 * @param WP_Post $p	Post object.
 *
 * @return array		List of ids.
 */
function bimber_get_post_gallery_media_ids($p ) {
	$ids = array();

	if ( $p = get_post( $p ) ) {
		if ( has_shortcode( $p->post_content, 'gallery' ) ) {
			if ( preg_match_all( '/' . get_shortcode_regex() . '/s', $p->post_content, $matches, PREG_SET_ORDER ) ) {
				// Get first [gallery] shortcode.
				foreach ( $matches as $shortcode ) {
					if ( 'gallery' === $shortcode[2] ) {
						// Ids set explicitly.
						if ( preg_match( '/ids="([^"]+)"/', $shortcode[0], $ids_matches ) ) {
							$ids = explode( ',', trim( $ids_matches[1] ) );
							// Ids not get, fetch number of post image attachments.
						} else {
							$ids = array_keys( get_attached_media( 'image', $p->ID ) );
						}
						break;
					}
				}
			}
		}
	}

	return $ids;
}

/**
 * Returns number of media items for the first [gallery] shortcode in post content
 *
 * @param WP_Post $post		Post object.
 *
 * @return int				Number of gallery images.
 */
function bimber_get_post_gallery_media_count($post ) {
	return count( bimber_get_post_gallery_media_ids( $post ) );
}

/**
 * Generate post pagination using built-in WP page links
 *
 * @param array    $posts           Array of posts.
 * @param WP_Query $wp_query        WP Query.
 *
 * @return array
 */
function bimber_post_pagination( $posts, $wp_query ) {
	/**
	 * Check if query is an instance of WP_Query.
	 * Some plugins, like BuddyPress may change it.
	 */
	if ( ! ( $wp_query instanceof WP_Query ) ) {
		return $posts;
	}

	// Apply only for the_content on a single post.
	if ( ! ( $wp_query->is_main_query() && $wp_query->is_singular() ) ) {
		return $posts;
	}

	foreach ( $posts as $post ) {
		$post_format = get_post_format( $post );

		if ( ! in_array( $post_format, array( 'gallery' ), true ) ) {
			continue;
		}

		if ( ! has_shortcode( $post->post_content, 'gallery' ) ) {
			continue;
		}

		// One gallery item per page.
		$gallery_ids = bimber_get_post_gallery_media_ids( $post );
		$pages = count( $gallery_ids );

		if ( $pages < 2 ) {
			continue;
		}

		// Remove first [gallery] shortcode.
		$post->post_content = preg_replace( '/\[gallery[^\[]*\]/s', '', $post->post_content, 1 );

		// Build pages.
		foreach ( $gallery_ids as $index => $media_id ) {

			$attachment = get_post( $media_id );

			$post->post_content .= '[caption width="600" align="aligncenter"]';
			$post->post_content .= wp_get_attachment_image( $media_id, 'large' );

			if ( trim( $attachment->post_excerpt ) ) {
				$post->post_content .= ' ' . $attachment->post_excerpt;
			}

			$post->post_content .= '[/caption]';

			// @todo ???
			//$post->post_content .= $attachment->post_content;

			// The <!--nextpage--> tag is a divider between two pages. Number of dividers = pages - 1.
			if ( $index < $pages - 1 ) {
				$post->post_content .= '<!--nextpage-->';
			}
		}
	}

	return $posts;
}
