<?php
/**
 * Page functions
 *
 * @license For the full license information, please view the Licensing folder
 * that was distributed with this source code.
 *
 * @package Bimber_Theme
 */

// Prevent direct script access.
if ( ! defined( 'ABSPATH' ) ) {
	die( 'No direct script access allowed' );
}

/**
 * Adjust the HTML markup of pagination links
 *
 * @param array $args Arguments.
 *
 * @return array
 */
function bimber_filter_wp_link_pages_args( $args ) {
	global $page, $numpages, $multipage, $more, $pagenow;

	$overview = bimber_get_theme_option( 'post', 'pagination_overview' );

	$nextpagelink = __( 'Next', 'bimber' );
	$previouspagelink = __( 'Previous', 'bimber' );

	$before = '';
	$before .= '<nav class="g1-pagination pagelinks">';

		if ( 'none' === $overview ) {
			$before .= '<p class="g1-pagination-label g1-pagination-label-none">' . esc_html__( 'Pages:', 'bimber' ) . '</p>';
		} elseif ( 'page_xofy' === $overview ) {
			$before .= '<p class="g1-pagination-label g1-pagination-label-xofy">' . esc_html( sprintf( __( 'Page %1$d of %2$d', 'bimber' ), $page, $numpages ) ) . '</p>';
		} else {
			$before .= '<p class="g1-pagination-label g1-pagination-label-links"><strong>' . esc_html__( 'Pages:', 'bimber' ) . '</strong></p>';
		}


		if ( 'arrow' === bimber_get_theme_option( 'post', 'pagination_adjacent_label' ) ) {
			$before .= '<ul class="g1-pagination-just-arrows">';
		} else {
			$before .= '<ul>';
		}

	$after = '';
		$after .= '</ul>';
	$after .= '</nav>';

	if ( 'adjacent_page' === bimber_get_theme_option( 'post', 'pagination_adjacent_label' ) ) {
		$nextpagelink       = __( 'Next page', 'bimber' );
		$previouspagelink   = __( 'Previous page', 'bimber' );
	}

	$args = array_merge(
		$args,
		array(
			'before'           => $before,
			'after'            => $after,
			'current_before'   => '<strong class="current">',
			'current_after'    => '</strong>',
			'link_before'      => '<span>',
			'link_after'       => '</span>',
			'next_or_number'   => 'next_and_number',
			'separator'        => '',
			'nextpagelink'     => esc_html( $nextpagelink ),
			'previouspagelink' => esc_html( $previouspagelink ),
		)
	);

	// Based on: http://www.velvetblues.com/web-development-blog/wordpress-number-next-previous-links-wp_link_pages/ .
	if ( 'next_and_number' === $args['next_or_number'] ) {
		$args['next_or_number'] = 'number';
		$prev                   = '';
		$next                   = '';
		if ( $multipage ) {
			if ( $more ) {
				// Previous element.
				$i = $page - 1;
				$is_prev_link = $i && $more;

				$prev .= $is_prev_link ? _wp_link_page( $i ) : '<a >';
				$prev .= $args['link_before'] . $args['previouspagelink'] . $args['link_after'] . '</a>';

				if ( 'button' === bimber_get_theme_option( 'post', 'pagination_adjacent_style' ) ) {
					if ( 'page_links' === $overview ) {
						$prev = str_replace( '<a ', '<a class="g1-arrow g1-arrow-left g1-arrow-simple prev" ', $prev );
					} else {
						$prev = str_replace( '<a ', '<a class="g1-arrow g1-arrow-xl g1-arrow-left g1-arrow-simple prev" ', $prev );
					}
				} else {
					$prev = str_replace( '<a ', '<a class="g1-delta g1-delta-1st g1-link g1-link-left prev" ', $prev );
				}

				if ( ! $is_prev_link ) {
					$prev = str_replace( array( '<a ', '</a>' ), array( '<span ', '</span>' ), $prev );
				}

				$prev = '<li class="g1-pagination-item-prev">' . $prev . '</li>';


				// Next element.
				$i = $page + 1;
				$is_next_link = $i <= $numpages && $more;

				$next .= $is_next_link ? _wp_link_page( $i ) : '<a >';
				$next .= $args['link_before'] . $args['nextpagelink'] . $args['link_after'] . '</a>';

				if ( 'button' === bimber_get_theme_option( 'post', 'pagination_adjacent_style' ) ) {
					if ( 'page_links' === $overview ) {
						$next = str_replace( '<a ', '<a class="g1-arrow g1-arrow-right g1-arrow-solid next" ', $next );
					} else {
						$next = str_replace( '<a ', '<a class="g1-arrow g1-arrow-xl g1-arrow-right g1-arrow-solid next" ', $next );
					}
				} else {
					$next = str_replace( '<a ', '<a class="g1-delta g1-delta-1st g1-link g1-link-right next" ', $next );
				}

				if ( ! $is_next_link ) {
					$next = str_replace( array( '<a ', '</a>' ), array( '<span ', '</span>' ), $next );
				}

				$next = '<li class="g1-pagination-item-next">' . $next . '</li>';
			}
		}
		$args['before'] = $args['before'] . $prev;
		$args['after']  = $next . $args['after'];
	}

	return $args;
}

/**
 * Add some markup to the output of the wp_link_pages_link function
 *
 * @param string $link Markup.
 * @param int    $i Index.
 *
 * @return string
 */
function bimber_filter_wp_link_pages_link( $link, $i ) {
	global $page, $numpages;

	if ( $i === $page ) {
		$link = '<li class="g1-pagination-item-current">' . $link . '</li>';
	} else {
		$link = '<li class="g1-pagination-item">' . $link . '</li>';
	}

	return $link;
}

/**
 * Append all collections (Trending/Hot/Popular) to the content of the Top page
 *
 * @param string $content Post content.
 *
 * @return string
 */
function bimber_top_page( $content ) {
	if ( bimber_is_top_page() ) {
		remove_filter( 'the_content', 'bimber_top_page', 11 );

		ob_start();
		get_template_part( 'template-parts/top-page' );
		$extra_content = ob_get_clean();

		add_filter( 'the_content', 'bimber_top_page', 11 );

		$content .= $extra_content;
	}

	return $content;
}

/**
 * Append the Hot entries collection to the content of the Hot page
 *
 * @param string $content Post content.
 *
 * @return string
 */
function bimber_list_hot_entries( $content ) {
	if ( bimber_is_hot_page() ) {
		remove_filter( 'the_content', 'bimber_list_hot_entries', 11 );

		ob_start();
		get_template_part( 'template-parts/collection-hot' );
		$extra_content = ob_get_clean();

		add_filter( 'the_content', 'bimber_list_hot_entries', 11 );

		$content .= $extra_content;
	}

	return $content;
}

/**
 * Append the Popular entries collection to the content of the Popular page
 *
 * @param string $content Post content.
 *
 * @return string
 */
function bimber_list_popular_entries( $content ) {
	if ( bimber_is_popular_page() ) {
		remove_filter( 'the_content', 'bimber_list_popular_entries', 11 );

		ob_start();
		get_template_part( 'template-parts/collection-popular' );
		$extra_content = ob_get_clean();

		add_filter( 'the_content', 'bimber_list_popular_entries', 11 );

		$content .= $extra_content;
	}

	return $content;
}

/**
 * Append the TrendingHot entries collection to the content of the Trending page
 *
 * @param string $content Post content.
 *
 * @return string
 */
function bimber_list_trending_entries( $content ) {
	if ( bimber_is_trending_page() ) {
		remove_filter( 'the_content', 'bimber_list_trending_entries', 11 );

		ob_start();
		get_template_part( 'template-parts/collection-trending' );
		$extra_content = ob_get_clean();

		add_filter( 'the_content', 'bimber_list_trending_entries', 11 );

		$content .= $extra_content;
	}

	return $content;
}
