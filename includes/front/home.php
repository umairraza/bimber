<?php
/**
 * Archive functions
 *
 * @license For the full license information, please view the Licensing folder
 * that was distributed with this source code.
 *
 * @package Bimber_Theme
 */

// Prevent direct script access.
if ( ! defined( 'ABSPATH' ) ) {
	die( 'No direct script access allowed' );
}


/**
 * Alters home template based on theme options
 *
 * @param string $template Template name.
 *
 * @return string
 */
function bimber_home_alter_template( $template ) {
	$home_settings = bimber_get_home_settings();

	$new_template = $home_settings['template'];
	$new_template = sprintf( 'g1-template-home-%s.php', $new_template );

	$new_template = locate_template( $new_template );

	if ( ! empty( $new_template ) ) {
		return $new_template;
	}

	return $template;
}

/**
 * Get home page settings.
 *
 * @return array
 */
function bimber_get_home_settings() {
	$featured_entries_category = bimber_get_theme_option( 'home', 'featured_entries_category' );

	if ( is_array( $featured_entries_category ) ) {
		$featured_entries_category = implode( ',', $featured_entries_category );
	}

	return apply_filters( 'bimber_home_settings', array(
		'template'         				=> bimber_get_theme_option( 'home', 'template' ),
		'title'            				=> bimber_get_home_title(),
		'pagination'       				=> bimber_get_theme_option( 'home', 'pagination' ),
		'elements'         				=> bimber_get_archive_elements_visibility_arr( bimber_get_theme_option( 'home', 'hide_elements' ) ),
		'featured_entries_title'		=> bimber_get_home_featured_entries_title(),
		'featured_entries_title_hide'	=> (bool) bimber_get_theme_option( 'home', 'featured_entries_title_hide' ),
		// Query args.
		'featured_entries' => array(
			'type'          => bimber_get_theme_option( 'home', 'featured_entries' ),
			'time_range'    => bimber_get_theme_option( 'home', 'featured_entries_time_range' ),
			'elements'      => bimber_get_archive_elements_visibility_arr( bimber_get_theme_option( 'home', 'featured_entries_hide_elements' ) ),
			'category_name' => $featured_entries_category,
			'tag_slug__in'  => array_filter( bimber_get_theme_option( 'home', 'featured_entries_tag' ) ),
		),
	) );
}

/**
 * Inject a newsletter sign-up form into the loop.
 *
 * @param string $template_type Classic, grid or list.
 * @param int    $post_number The current position in the loop.
 */
function bimber_home_inject_newsletter_into_loop( $template_type, $post_number ) {
	$inject = bimber_get_theme_option( 'home', 'newsletter' ) === 'standard';

	if ( ! $inject ) {
		return;
	}

	$inject_after_post = absint( bimber_get_theme_option( 'home', 'newsletter_after_post' ) );

	$posts_per_page = (int) get_option( 'posts_per_page' );
	$current_page   = absint( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
	$page_offset    = ( $current_page - 1 ) * $posts_per_page;

	$post_preceding_newsletter_found           = ( $post_number + $page_offset === $inject_after_post );
	$injected_newsletter_still_on_current_page = ( $posts_per_page + $page_offset > $inject_after_post );

	if ( $post_preceding_newsletter_found && $injected_newsletter_still_on_current_page ) {
		get_template_part( 'template-parts/newsletter-inside-' . $template_type );
	}
}

/**
 * Inject an advertisement into the home loop.
 *
 * @param string $template_type Classic, grid or list.
 * @param int    $post_number The current position in the loop.
 */
function bimber_home_inject_ad_into_loop( $template_type, $post_number ) {
	$inject = bimber_get_theme_option( 'home', 'ad' ) === 'standard';

	if ( ! $inject ) {
		return;
	}

	$inject_after_post = absint( bimber_get_theme_option( 'home', 'ad_after_post' ) );

	$posts_per_page = (int) get_option( 'posts_per_page' );
	$current_page   = absint( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
	$page_offset    = ( $current_page - 1 ) * $posts_per_page;

	$post_preceding_ad_found           = ( $post_number + $page_offset === $inject_after_post );
	$injected_ad_still_on_current_page = ( $posts_per_page + $page_offset > $inject_after_post );

	if ( $post_preceding_ad_found && $injected_ad_still_on_current_page ) {
		get_template_part( 'template-parts/ad-inside-' . $template_type );
	}
}

/**
 * Set maximum number of entries to show on the home page.
 *
 * @param WP_Query $query Home main query.
 */
function bimber_home_set_posts_per_page( $query ) {
	if ( is_admin() || ! $query->is_main_query() ) {
		return;
	}

	if ( ! is_home() ) {
		return;
	}

	$posts_per_page = (int) get_option( 'posts_per_page' );
	$offset         = $query->get( 'offset' );

	if ( $posts_per_page <= 0 ) {
		return;
	}

	$query->set( 'bimber_posts_per_page', $posts_per_page );

	$current_page   = absint( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
	$list_min_index = ( $current_page - 1 ) * $posts_per_page;
	$list_max_index = $list_min_index + $posts_per_page;

	$inject_newsletter = bimber_get_theme_option( 'home', 'newsletter' ) === 'standard';

	// Count "newsletter" as an item.
	if ( $inject_newsletter ) {
		$inject_after_post = absint( bimber_get_theme_option( 'home', 'newsletter_after_post' ) );

		$is_inside_the_list         = ( $inject_after_post > $list_min_index ) && ( $inject_after_post < $list_max_index );
		$was_injected_on_prev_pages = $inject_after_post < $list_min_index;

		if ( $is_inside_the_list ) {
			// Offset and posts_per_page work together, so if want to change posts_per_page, we need init the offset first.
			if ( empty( $offset ) ) {
				$offset = $list_min_index;
			}

			$posts_per_page --;
		} elseif ( $was_injected_on_prev_pages ) {
			if ( empty( $offset ) ) {
				$offset = $list_min_index;
			}

			$offset --;
		}
	}

	// Count "ad" as an item.
	$inject_ad = bimber_get_theme_option( 'home', 'ad' ) === 'standard';

	if ( $inject_ad ) {
		$inject_after_post = absint( bimber_get_theme_option( 'home', 'ad_after_post' ) );

		$is_inside_the_list         = ( $inject_after_post > $list_min_index ) && ( $inject_after_post < $list_max_index );
		$was_injected_on_prev_pages = $inject_after_post < $list_min_index;

		// Ad must be inside the list.
		if ( $is_inside_the_list ) {
			// Offset and posts_per_page work together, so if want to change posts_per_page, we need init the offset first.
			if ( empty( $offset ) ) {
				$offset = $list_min_index;
			}

			$posts_per_page --;
		} elseif ( $was_injected_on_prev_pages ) {
			if ( empty( $offset ) ) {
				$offset = $list_min_index;
			}
			$offset --;
		}
	}

	$query->set( 'posts_per_page', $posts_per_page );
	$query->set( 'offset', $offset );
}

/**
 * Adjust the home pagination.
 *
 * @param int      $found_posts Number of found posts.
 * @param WP_Query $query Home main query.
 *
 * @return mixed
 */
function bimber_home_adjust_offset_pagination( $found_posts, $query ) {
	if ( is_admin() || ! $query->is_main_query() ) {
		return $found_posts;
	}

	if ( ! is_home() ) {
		return $found_posts;
	}

	$posts_per_page = (int) get_option( 'posts_per_page' );

	if ( $posts_per_page <= 0 ) {
		return $found_posts;
	}

	$extra_posts = 0;

	// Newsletter.
	$inject_newsletter = bimber_get_theme_option( 'home', 'newsletter' ) === 'standard';

	if ( $inject_newsletter ) {
		$newsletter_after_post = absint( bimber_get_theme_option( 'home', 'newsletter_after_post' ) );

		if ( $newsletter_after_post <= $found_posts ) {
			$extra_posts++;
		}
	}

	// Ad.
	$inject_ad = bimber_get_theme_option( 'home', 'ad' ) === 'standard';

	if ( $inject_ad ) {
		$ad_after_post = absint( bimber_get_theme_option( 'home', 'ad_after_post' ) );

		if ( $ad_after_post <= $found_posts ) {
			$extra_posts++;
		}
	}

	$found_posts += $extra_posts;

	return $found_posts;
}

/**
 * Get featured post ids.
 *
 * @return array
 */
function bimber_get_home_featured_posts_ids() {
	$home_settings    = bimber_get_home_settings();
	$featured_entries = $home_settings['featured_entries'];

	if ( 'none' === $featured_entries['type'] ) {
		return array();
	}

	$featured_entries['posts_per_page'] = bimber_get_post_per_page_from_template( $home_settings['template'] );

	return bimber_get_featured_posts_ids( $featured_entries );
}

/**
 * Exclude the featured content from the home main query.
 *
 * @param WP_Query $query Home main query.
 */
function bimber_home_exclude_featured( $query ) {
	if ( ! $query->is_main_query() || is_feed() ) {
		return;
	}

	if ( ! is_home() ) {
		return;
	}

	$excluded_ids = bimber_get_home_featured_posts_ids();

	if ( bimber_show_global_featured_entries() && bimber_global_featured_entries_exclude_from_main_loop() ) {
		$global_featured_ids = bimber_get_global_featured_posts_ids();

		if ( ! empty( $global_featured_ids ) ) {
			$excluded_ids = array_merge( $excluded_ids, $global_featured_ids );

			$excluded_ids = array_unique( $excluded_ids );
		}
	}

	if ( ! empty( $excluded_ids ) ) {
		$query->set( 'post__not_in', $excluded_ids );

		// When we exclude posts from main query, it can be left empty.
		// We don't want to show empty loop info because featured entries are there.
		add_filter( 'bimber_show_archive_no_results', '__return_false' );
	}
}

/**
 * Get the title of the home collection.
 *
 * @return string
 */
function bimber_get_home_title() {
	$title = bimber_get_theme_option( 'home', 'title' );

	// Fallback to defaults.
	if ( ! strlen( $title ) ) {
		if ( 'recent' === bimber_get_theme_option( 'home', 'featured_entries' ) ) {
			$title = __( 'More stories', 'bimber' );
		} else {
			$title = __( 'Latest stories', 'bimber' );
		}
	}

	return $title;
}

/**
 * Get the title of the home featured entries.
 *
 * @return string
 */
function bimber_get_home_featured_entries_title() {
	$title = bimber_get_theme_option( 'home', 'featured_entries_title' );

	// Fallback to defaults.
	if ( ! strlen( $title ) ) {
		$type = bimber_get_theme_option( 'home', 'featured_entries' );

		switch ( $type ) {
			case 'most_viewed':
				$title = __( 'Most viewed', 'bimber' );
				break;

			case 'most_shared':
				$title = __( 'Most shared', 'bimber' );
				break;

			default:
				$title = __( 'Latest stories', 'bimber' );
		}
	}

	return $title;
}
