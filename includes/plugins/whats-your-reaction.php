<?php
/**
 * What's Your Reaction? plugin functions
 *
 * @license For the full license information, please view the Licensing folder
 * that was distributed with this source code.
 *
 * @package Bimber_Theme
 */

// Prevent direct script access.
if ( ! defined( 'ABSPATH' ) ) {
	die( 'No direct script access allowed' );
}

function bimber_wyr_load_post_voting_box() {
	if ( apply_filters( 'bimber_wyr_load_post_voting_box', is_single() ) ) {
		wyr_render_voting_box();
	}
}