<?php
/**
 * Admin hooks
 *
 * @license For the full license information, please view the Licensing folder
 * that was distributed with this source code.
 *
 * @package Bimber_Theme
 */

// Prevent direct script access.
if ( ! defined( 'ABSPATH' ) ) {
	die( 'No direct script access allowed' );
}

// Post.
add_filter( 'manage_posts_columns',         'bimber_post_list_add_id_column' );
add_action( 'manage_posts_custom_column',   'bimber_post_list_render_id_column' );
add_filter( 'manage_posts_columns',         'bimber_post_list_custom_columns' );
add_action( 'manage_posts_custom_column',   'bimber_post_list_custom_columns_data', 10, 2 );

// Enqueue assets.
add_action( 'admin_enqueue_scripts', 'bimber_admin_enqueue_styles' );
add_action( 'admin_enqueue_scripts', 'bimber_admin_enqueue_scripts' );

// Ajax.
add_action( 'wp_ajax_bimber_change_mode_to_normal',         'bimber_ajax_change_mode_to_normal' );
add_action( 'wp_ajax_bimber_change_mode_to_in_progress',    'bimber_ajax_change_mode_to_in_progress' );

// Theme Activation.
add_action( 'after_switch_theme',                   'bimber_redirect_after_activation' );
add_action( 'after_switch_theme',                   'bimber_reset_tgm_notices' );
add_action( 'load-appearance_page_theme-options',   'bimber_load_welcome_page_until_installation_complete' );

// TGM.
add_action( 'tgmpa_register', 'bimber_register_required_plugins' );
add_action( 'bimber_tgm_plugins_config', 'bimber_register_wordpress_importer' );

// Dynamic style cache.
add_action( 'customize_save', 'bimber_dynamic_style_mark_cache_as_stale' );
add_action( 'update_option_' . bimber_get_theme_options_id(), 'bimber_dynamic_style_theme_option_changed', 999, 2 );

// Styles.
add_action( 'admin_init', 'bimber_add_editor_styles' );

// Demo content.
add_action( 'admin_init', 'bimber_handle_import_action' );
add_action( 'admin_action_bimber_import_demo', 'bimber_import_demo' );

// Cache.
add_action( 'save_post',        'bimber_delete_transients' );   // Fires once a post has been saved.
add_action( 'deleted_post',     'bimber_delete_transients' );   // Fires immediately after a post is deleted from the database.
add_action( 'switch_theme',     'bimber_delete_transients' );   // Fires once user activate/deactivate the theme.
add_action( 'customize_save', 	'bimber_delete_transients' );	// Fires once settings are published in WP Customizer.

// About.
add_action( 'admin_menu', 'bimber_register_about_page' );
