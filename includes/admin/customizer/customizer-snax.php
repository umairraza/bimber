<?php
/**
 * WP Customizer panel section to handle featured entries options
 *
 * @license For the full license information, please view the Licensing folder
 * that was distributed with this source code.
 *
 * @package Bimber_Theme
 */

// Prevent direct script access.
if ( ! defined( 'ABSPATH' ) ) {
	die( 'No direct script access allowed' );
}

$bimber_option_name = bimber_get_theme_id();

$wp_customize->add_section( 'bimber_snax_section', array(
	'title'    => esc_html__( 'Snax', 'bimber' ),
	'priority' => 500,
) );

// Featured Entries.
$wp_customize->add_setting( $bimber_option_name . '[snax_header_type]', array(
	'default'           => $bimber_customizer_defaults['snax_header_type'],
	'type'              => 'option',
	'capability'        => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_text_field',
) );

$wp_customize->add_control( 'bimber_snax_header_type', array(
	'label'    => esc_html__( 'Header on Submission Pages', 'bimber' ),
	'section'  => 'bimber_snax_section',
	'settings' => $bimber_option_name . '[snax_header_type]',
	'type'     => 'select',
	'choices'  => array(
		'normal'	=> esc_html__( 'Normal', 'bimber' ),
		'simple'	=> esc_html__( 'Simplified', 'bimber' ),
	),
) );
