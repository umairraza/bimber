<?php
/**
 * WP Customizer panel section to handle homepage options
 *
 * @license For the full license information, please view the Licensing folder
 * that was distributed with this source code.
 *
 * @package Bimber_Theme
 */

// Prevent direct script access.
if ( ! defined( 'ABSPATH' ) ) {
	die( 'No direct script access allowed' );
}

$bimber_option_name = bimber_get_theme_id();

// Show info about currently previewing page.
$wp_customize->add_setting( 'bimber_on_posts_page_info', array(
	'capability'        => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_text_field',
) );

$wp_customize->add_control( new Bimber_Customize_HTML_Control( $wp_customize, 'bimber_on_posts_page_info', array(
	'section'         => 'static_front_page',
	'settings'        => 'bimber_on_posts_page_info',
	'html'            =>
		'<p class="g1-customizer-on-page-info">
			<strong>' . esc_html__( 'You\'re previewing the posts page.', 'bimber' ) . '</strong>
		</p>',
	'active_callback' => 'is_home',
) ) );


// Template.
$wp_customize->add_setting( $bimber_option_name . '[home_template]', array(
	'default'           => $bimber_customizer_defaults['home_template'],
	'type'              => 'option',
	'capability'        => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_text_field',
) );

$wp_customize->add_control( 'bimber_home_template', array(
	'label'           => esc_html__( 'Template', 'bimber' ),
	'section'         => 'static_front_page',
	'settings'        => $bimber_option_name . '[home_template]',
	'type'            => 'select',
	'choices'         => array(
		'one-featured-list-sidebar'      => esc_html__( 'List with Sidebar, 1 Featured', 'bimber' ),
		'three-featured-list-sidebar'    => esc_html__( 'List with Sidebar, 3 Featured', 'bimber' ),
		'one-featured-grid-sidebar'      => esc_html__( 'Grid with Sidebar, 1 Featured', 'bimber' ),
		'three-featured-grid-sidebar'    => esc_html__( 'Grid with Sidebar, 3 Featured', 'bimber' ),
		'two-featured-grid'              => esc_html__( 'Grid, 2 Featured', 'bimber' ),
		'three-featured-grid'            => esc_html__( 'Grid, 3 Featured', 'bimber' ),
		'one-featured-classic-sidebar'   => esc_html__( 'Classic with Sidebar, 1 Featured', 'bimber' ),
		'three-featured-classic-sidebar' => esc_html__( 'Classic with Sidebar, 3 Featured', 'bimber' ),
		'three-featured-stream-sidebar'  => esc_html__( 'Stream with Sidebar, 3 Featured', 'bimber' ),
		'three-featured-stream'          => esc_html__( 'Stream, 3 Featured', 'bimber' ),
	),
	'active_callback' => 'bimber_customizer_is_posts_page_selected',
) );

/**
 * Check whether user chose page for Posts
 *
 * @param WP_Customize_Control $control     Control instance for which this callback is executed.
 *
 * @return bool
 */
function bimber_customizer_is_posts_page_selected( $control ) {
	$show_on_front = $control->manager->get_setting( 'show_on_front' )->value();

	// Front page displays.
	if ( 'posts' === $show_on_front ) {
		// Your Latest posts.
		return true;
	} else {
		// A static page.
		$page_for_posts = $control->manager->get_setting( 'page_for_posts' )->value();

		// A page is selected (0 means no selection).
		return '0' !== $page_for_posts;
	}
}


// Featured Entries.
$wp_customize->add_setting( $bimber_option_name . '[home_featured_entries]', array(
	'default'           => $bimber_customizer_defaults['home_featured_entries'],
	'type'              => 'option',
	'capability'        => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_text_field',
) );

$wp_customize->add_control( 'bimber_home_featured_entries', array(
	'label'    => esc_html__( 'Featured Entries', 'bimber' ),
	'section'  => 'static_front_page',
	'settings' => $bimber_option_name . '[home_featured_entries]',
	'type'     => 'select',
	'choices'  => array(
		'most_shared' => esc_html__( 'most shared', 'bimber' ),
		'most_viewed' => esc_html__( 'most viewed', 'bimber' ),
		'recent'      => esc_html__( 'recent', 'bimber' ),
		'none'        => esc_html__( 'none', 'bimber' ),
	),
) );

/**
 * Check whether featured entries are enabled for homepage
 *
 * @param WP_Customize_Control $control     Control instance for which this callback is executed.
 *
 * @return bool
 */
function bimber_customizer_home_has_featured_entries( $control ) {
	if ( ! bimber_customizer_is_posts_page_selected( $control ) ) {
		return false;
	}

	$type = $control->manager->get_setting( bimber_get_theme_id() . '[home_featured_entries]' )->value();

	return 'none' !== $type;
}

/**
 * Check whether featured entries tag filter is supported
 *
 * @param WP_Customize_Control $control     Control instance for which this callback is executed.
 *
 * @return bool
 */
function bimber_customizer_home_featured_entries_tag_is_active( $control ) {
	$has_featured_entries = bimber_customizer_home_has_featured_entries( $control );

	// Skip if home doesn't use the Featured Entries.
	if ( ! $has_featured_entries ) {
		return false;
	}

	$featured_entries_type = $control->manager->get_setting( bimber_get_theme_id() . '[home_featured_entries]' )->value();

	// The most viewed types doesn't support tag filter.
	if ( 'most_viewed' === $featured_entries_type ) {
		return false;
	}

	return true;
}

// Featured entries title.
$wp_customize->add_setting( $bimber_option_name . '[home_featured_entries_title]', array(
	'default'           => $bimber_customizer_defaults['home_featured_entries_title'],
	'type'              => 'option',
	'capability'        => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_text_field',
) );

$wp_customize->add_control( 'bimber_home_featured_entries_title', array(
	'label'           => esc_html__( 'Featured Entries Title', 'bimber' ),
	'section'         => 'static_front_page',
	'settings'        => $bimber_option_name . '[home_featured_entries_title]',
	'type'            => 'text',
	'input_attrs'     => array(
		'placeholder' => esc_html__( 'Leave empty to use default', 'bimber' ),
	),
	'active_callback' => 'bimber_customizer_home_has_featured_entries',
) );

// Featured entries hide title.
$wp_customize->add_setting( $bimber_option_name . '[home_featured_entries_title_hide]', array(
	'default'           => $bimber_customizer_defaults['home_featured_entries_title_hide'],
	'type'              => 'option',
	'capability'        => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_text_field',
) );

$wp_customize->add_control( 'bimber_home_featured_entries_title_hide', array(
	'label'    => esc_html__( 'Featured Entries Hide Title', 'bimber' ),
	'section'  => 'static_front_page',
	'settings' => $bimber_option_name . '[home_featured_entries_title_hide]',
	'type'     => 'checkbox',
	'active_callback' => 'bimber_customizer_home_has_featured_entries',
) );

// Category.
$wp_customize->add_setting( $bimber_option_name . '[home_featured_entries_category]', array(
	'default'           => $bimber_customizer_defaults['home_featured_entries_category'],
	'type'              => 'option',
	'capability'        => 'edit_theme_options',
	'sanitize_callback' => 'bimber_sanitize_multi_choice',
) );

$wp_customize->add_control( new Bimber_Customize_Multi_Checkbox_Control( $wp_customize, 'bimber_home_featured_entries_category', array(
	'label'           => esc_html__( 'Featured Entries Category', 'bimber' ),
	'section'         => 'static_front_page',
	'settings'        => $bimber_option_name . '[home_featured_entries_category]',
	'choices'         => bimber_customizer_get_category_choices(),
	'active_callback' => 'bimber_customizer_home_has_featured_entries',
) ) );


// Tag.
$wp_customize->add_setting( $bimber_option_name . '[home_featured_entries_tag]', array(
	'default'           => $bimber_customizer_defaults['home_featured_entries_tag'],
	'type'              => 'option',
	'capability'        => 'edit_theme_options',
	'sanitize_callback' => 'bimber_sanitize_multi_choice',
) );

$wp_customize->add_control( new Bimber_Customize_Multi_Select_Control( $wp_customize, 'bimber_home_featured_entries_tag', array(
	'label'           => esc_html__( 'Featured Entries Tag', 'bimber' ),
	'description'     => esc_html__( 'you can choose many', 'bimber' ),
	'section'         => 'static_front_page',
	'settings'        => $bimber_option_name . '[home_featured_entries_tag]',
	'choices'         => bimber_customizer_get_tag_choices(),
	'active_callback' => 'bimber_customizer_home_featured_entries_tag_is_active',
) ) );


// Featured Entries Time range.
$wp_customize->add_setting( $bimber_option_name . '[home_featured_entries_time_range]', array(
	'default'           => $bimber_customizer_defaults['home_featured_entries_time_range'],
	'type'              => 'option',
	'capability'        => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_text_field',
) );

$wp_customize->add_control( 'bimber_home_featured_entries_time_range', array(
	'label'           => esc_html__( 'Featured Entries Time range', 'bimber' ),
	'section'         => 'static_front_page',
	'settings'        => $bimber_option_name . '[home_featured_entries_time_range]',
	'type'            => 'select',
	'choices'         => array(
		'day'   => esc_html__( 'last 24 hours', 'bimber' ),
		'week'  => esc_html__( 'last 7 days', 'bimber' ),
		'month' => esc_html__( 'last 30 days', 'bimber' ),
		'all'   => esc_html__( 'all time', 'bimber' ),
	),
	'active_callback' => 'bimber_customizer_home_has_featured_entries',
) );


// Featured Entries Hide Elements.
$wp_customize->add_setting( $bimber_option_name . '[home_featured_entries_hide_elements]', array(
	'default'           => $bimber_customizer_defaults['home_featured_entries_hide_elements'],
	'type'              => 'option',
	'capability'        => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_text_field',
) );

$wp_customize->add_control( new Bimber_Customize_Multi_Checkbox_Control( $wp_customize, 'bimber_home_featured_entries_hide_elements', array(
	'label'           => esc_html__( 'Featured Entries Hide Elements', 'bimber' ),
	'section'         => 'static_front_page',
	'settings'        => $bimber_option_name . '[home_featured_entries_hide_elements]',
	'choices'         => array(
		'categories'    => esc_html__( 'Categories', 'bimber' ),
		'shares'        => esc_html__( 'Shares', 'bimber' ),
		'views'         => esc_html__( 'Views', 'bimber' ),
		'comments_link' => esc_html__( 'Comments Link', 'bimber' ),
	),
	'active_callback' => 'bimber_customizer_home_has_featured_entries',
) ) );


// Title.
$wp_customize->add_setting( $bimber_option_name . '[home_title]', array(
	'default'           => $bimber_customizer_defaults['home_title'],
	'type'              => 'option',
	'capability'        => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_text_field',
) );

$wp_customize->add_control( 'bimber_home_title', array(
	'label'           => esc_html__( 'Title', 'bimber' ),
	'section'         => 'static_front_page',
	'settings'        => $bimber_option_name . '[home_title]',
	'type'            => 'text',
	'input_attrs'     => array(
		'placeholder' => esc_html__( 'Leave empty to use default', 'bimber' ),
	),
) );

// Hide title.
$wp_customize->add_setting( $bimber_option_name . '[home_title_hide]', array(
	'default'           => $bimber_customizer_defaults['home_title_hide'],
	'type'              => 'option',
	'capability'        => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_text_field',
) );

$wp_customize->add_control( 'bimber_home_title_hide', array(
	'label'    => esc_html__( 'Hide title', 'bimber' ),
	'section'  => 'static_front_page',
	'settings' => $bimber_option_name . '[home_title_hide]',
	'type'     => 'checkbox',
) );


// Posts Per Page.
$wp_customize->add_setting( 'posts_per_page', array(
	'type'              => 'option',
	'capability'        => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_text_field',
) );

$wp_customize->add_control( 'bimber_home_posts_per_page', array(
	'label'    => esc_html__( 'Entries per page', 'bimber' ),
	'section'  => 'static_front_page',
	'settings' => 'posts_per_page',
	'type'     => 'number',
) );


// Pagination.
$wp_customize->add_setting( $bimber_option_name . '[home_pagination]', array(
	'default'           => $bimber_customizer_defaults['home_pagination'],
	'type'              => 'option',
	'capability'        => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_text_field',
) );

$wp_customize->add_control( 'bimber_home_pagination', array(
	'label'    => esc_html__( 'Pagination', 'bimber' ),
	'section'  => 'static_front_page',
	'settings' => $bimber_option_name . '[home_pagination]',
	'type'     => 'select',
	'choices'  => array(
		'load-more'                 => esc_html__( 'Load More', 'bimber' ),
		'infinite-scroll'           => esc_html__( 'Infinite Scroll', 'bimber' ),
		'infinite-scroll-on-demand' => esc_html__( 'Infinite Scroll (first load via click)', 'bimber' ),
		'pages'                     => esc_html__( 'Prev/Next Pages', 'bimber' ),
	),
) );


// Hide Elements.
$wp_customize->add_setting( $bimber_option_name . '[home_hide_elements]', array(
	'default'           => $bimber_customizer_defaults['home_hide_elements'],
	'type'              => 'option',
	'capability'        => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_text_field',
) );

$wp_customize->add_control( new Bimber_Customize_Multi_Checkbox_Control( $wp_customize, 'bimber_home_hide_elements', array(
	'label'    => esc_html__( 'Hide Elements', 'bimber' ),
	'section'  => 'static_front_page',
	'settings' => $bimber_option_name . '[home_hide_elements]',
	'choices'  => array(
		'featured_media' => esc_html__( 'Featured Media', 'bimber' ),
		'categories'     => esc_html__( 'Categories', 'bimber' ),
		'summary'        => esc_html__( 'Summary', 'bimber' ),
		'author'         => esc_html__( 'Author', 'bimber' ),
		'avatar'         => esc_html__( 'Avatar', 'bimber' ),
		'date'           => esc_html__( 'Date', 'bimber' ),
		'shares'         => esc_html__( 'Shares', 'bimber' ),
		'views'          => esc_html__( 'Views', 'bimber' ),
		'comments_link'  => esc_html__( 'Comments Link', 'bimber' ),
	),
) ) );


// Newsletter.
$wp_customize->add_setting( $bimber_option_name . '[home_newsletter]', array(
	'default'           => $bimber_customizer_defaults['home_newsletter'],
	'type'              => 'option',
	'capability'        => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_text_field',
) );

$wp_customize->add_control( 'bimber_home_newsletter', array(
	'label'           => esc_html__( 'Newsletter', 'bimber' ),
	'section'         => 'static_front_page',
	'settings'        => $bimber_option_name . '[home_newsletter]',
	'type'            => 'select',
	'choices'         => array(
		'standard' => esc_html__( 'inject into post collection', 'bimber' ),
		'none'     => esc_html__( 'hide', 'bimber' ),
	),
	'active_callback' => 'bimber_customizer_is_posts_page_selected',
) );

$wp_customize->add_setting( $bimber_option_name . '[home_newsletter_after_post]', array(
	'default'           => $bimber_customizer_defaults['home_newsletter_after_post'],
	'type'              => 'option',
	'capability'        => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_text_field',
) );

$wp_customize->add_control( 'bimber_home_newsletter_after_post', array(
	'label'           => esc_html__( 'Inject newsletter after post', 'bimber' ),
	'section'         => 'static_front_page',
	'settings'        => $bimber_option_name . '[home_newsletter_after_post]',
	'type'            => 'number',
	'input_attrs'     => array(
		'placeholder' => esc_html__( 'eg. 2', 'bimber' ),
	),
	'active_callback' => 'bimber_customizer_is_home_newsletter_checked',
) );

/**
 * Check whether newsletter is enabled for homepage
 *
 * @param WP_Customize_Control $control     Control instance for which this callback is executed.
 *
 * @return bool
 */
function bimber_customizer_is_home_newsletter_checked( $control ) {
	if ( ! bimber_customizer_is_posts_page_selected( $control ) ) {
		return false;
	}

	return $control->manager->get_setting( bimber_get_theme_id() . '[home_newsletter]' )->value() === 'standard';
}


// Ad.
$wp_customize->add_setting( $bimber_option_name . '[home_ad]', array(
	'default'           => $bimber_customizer_defaults['home_ad'],
	'type'              => 'option',
	'capability'        => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_text_field',
) );

$wp_customize->add_control( 'bimber_home_ad', array(
	'label'           => esc_html__( 'Ad', 'bimber' ),
	'section'         => 'static_front_page',
	'settings'        => $bimber_option_name . '[home_ad]',
	'type'            => 'select',
	'choices'         => array(
		'standard' => esc_html__( 'inject into post collection', 'bimber' ),
		'none'     => esc_html__( 'hide', 'bimber' ),
	),
	'active_callback' => 'bimber_customizer_is_posts_page_selected',
) );

$wp_customize->add_setting( $bimber_option_name . '[home_ad_after_post]', array(
	'default'           => $bimber_customizer_defaults['home_ad_after_post'],
	'type'              => 'option',
	'capability'        => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_text_field',
) );

$wp_customize->add_control( 'bimber_home_ad_after_post', array(
	'label'           => esc_html__( 'Inject ad after post', 'bimber' ),
	'section'         => 'static_front_page',
	'settings'        => $bimber_option_name . '[home_ad_after_post]',
	'type'            => 'text',
	'input_attrs'     => array(
		'placeholder' => esc_html__( 'eg. 4', 'bimber' ),
	),
	'active_callback' => 'bimber_customizer_is_home_ad_checked',
) );

/**
 * Check whether ad is enabled for homepage
 *
 * @param WP_Customize_Control $control     Control instance for which this callback is executed.
 *
 * @return bool
 */
function bimber_customizer_is_home_ad_checked( $control ) {
	if ( ! bimber_customizer_is_posts_page_selected( $control ) ) {
		return false;
	}

	return $control->manager->get_setting( bimber_get_theme_id() . '[home_ad]' )->value() === 'standard';
}
