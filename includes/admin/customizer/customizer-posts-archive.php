<?php
/**
 * WP Customizer panel section to handle posts archive options
 *
 * @license For the full license information, please view the Licensing folder
 * that was distributed with this source code.
 *
 * @package Bimber_Theme
 */

// Prevent direct script access.
if ( ! defined( 'ABSPATH' ) ) {
	die( 'No direct script access allowed' );
}

$bimber_option_name = bimber_get_theme_id();

$wp_customize->add_section( 'bimber_posts_archive_section', array(
	'title'    => esc_html__( 'Archive', 'bimber' ),
	'priority' => 40,
	'panel'    => 'bimber_posts_panel',
) );


// Template.
$wp_customize->add_setting( $bimber_option_name . '[archive_template]', array(
	'default'           => $bimber_customizer_defaults['archive_template'],
	'type'              => 'option',
	'capability'        => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_text_field',
) );

$wp_customize->add_control( 'bimber_archive_template', array(
	'label'    => esc_html__( 'Template', 'bimber' ),
	'section'  => 'bimber_posts_archive_section',
	'settings' => $bimber_option_name . '[archive_template]',
	'type'     => 'select',
	'choices'  => array(
		'one-featured-list-sidebar'      => esc_html__( 'List with Sidebar, 1 Featured', 'bimber' ),
		'three-featured-list-sidebar'    => esc_html__( 'List with Sidebar, 3 Featured', 'bimber' ),
		'one-featured-grid-sidebar'      => esc_html__( 'Grid with Sidebar, 1 Featured', 'bimber' ),
		'three-featured-grid-sidebar'    => esc_html__( 'Grid with Sidebar, 3 Featured', 'bimber' ),
		'three-featured-grid'            => esc_html__( 'Grid, 3 Featured', 'bimber' ),
		'one-featured-classic-sidebar'   => esc_html__( 'Classic with Sidebar, 1 Featured', 'bimber' ),
		'three-featured-classic-sidebar' => esc_html__( 'Classic with Sidebar, 3 Featured', 'bimber' ),
		'three-featured-stream-sidebar'  => esc_html__( 'Stream with Sidebar, 3 Featured', 'bimber' ),
		'three-featured-stream'          => esc_html__( 'Stream, 3 Featured', 'bimber' ),
	),
) );


// Featured Entries.
$wp_customize->add_setting( $bimber_option_name . '[archive_featured_entries]', array(
	'default'           => $bimber_customizer_defaults['archive_featured_entries'],
	'type'              => 'option',
	'capability'        => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_text_field',
) );

$wp_customize->add_control( 'bimber_archive_featured_entries', array(
	'label'    => esc_html__( 'Featured Entries', 'bimber' ),
	'section'  => 'bimber_posts_archive_section',
	'settings' => $bimber_option_name . '[archive_featured_entries]',
	'type'     => 'select',
	'choices'  => array(
		'most_shared' => esc_html__( 'most shared', 'bimber' ),
		'most_viewed' => esc_html__( 'most viewed', 'bimber' ),
		'recent'      => esc_html__( 'recent', 'bimber' ),
		'none'        => esc_html__( 'none', 'bimber' ),
	),
) );


// Featured entries title.
$wp_customize->add_setting( $bimber_option_name . '[archive_featured_entries_title]', array(
	'default'           => $bimber_customizer_defaults['archive_featured_entries_title'],
	'type'              => 'option',
	'capability'        => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_text_field',
) );

$wp_customize->add_control( 'bimber_archive_featured_entries_title', array(
	'label'           => esc_html__( 'Featured Entries Title', 'bimber' ),
	'section'         => 'bimber_posts_archive_section',
	'settings'        => $bimber_option_name . '[archive_featured_entries_title]',
	'type'            => 'text',
	'input_attrs'     => array(
		'placeholder' => esc_html__( 'Leave empty to use default', 'bimber' ),
	),
	'active_callback' => 'bimber_customizer_archive_has_featured_entries',
) );

// Featured entries hide title.
$wp_customize->add_setting( $bimber_option_name . '[archive_featured_entries_title_hide]', array(
	'default'           => $bimber_customizer_defaults['archive_featured_entries_title_hide'],
	'type'              => 'option',
	'capability'        => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_text_field',
) );

$wp_customize->add_control( 'bimber_archive_featured_entries_title_hide', array(
	'label'    => esc_html__( 'Featured Entries Hide Title', 'bimber' ),
	'section'  => 'bimber_posts_archive_section',
	'settings' => $bimber_option_name . '[archive_featured_entries_title_hide]',
	'type'     => 'checkbox',
	'active_callback' => 'bimber_customizer_archive_has_featured_entries',
) );


// Featured Entries Time range.
$wp_customize->add_setting( $bimber_option_name . '[archive_featured_entries_time_range]', array(
	'default'           => $bimber_customizer_defaults['archive_featured_entries_time_range'],
	'type'              => 'option',
	'capability'        => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_text_field',
) );

$wp_customize->add_control( 'bimber_archive_featured_entries_time_range', array(
	'label'           => esc_html__( 'Featured Entries Time range', 'bimber' ),
	'section'         => 'bimber_posts_archive_section',
	'settings'        => $bimber_option_name . '[archive_featured_entries_time_range]',
	'type'            => 'select',
	'choices'         => array(
		'day'   => esc_html__( 'last 24 hours', 'bimber' ),
		'week'  => esc_html__( 'last 7 days', 'bimber' ),
		'month' => esc_html__( 'last 30 days', 'bimber' ),
		'all'   => esc_html__( 'all time', 'bimber' ),
	),
	'active_callback' => 'bimber_customizer_archive_has_featured_entries',
) );


// Featured Entries Hide Elements.
$wp_customize->add_setting( $bimber_option_name . '[archive_featured_entries_hide_elements]', array(
	'default'           => $bimber_customizer_defaults['archive_featured_entries_hide_elements'],
	'type'              => 'option',
	'capability'        => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_text_field',
) );

$wp_customize->add_control( new Bimber_Customize_Multi_Checkbox_Control( $wp_customize, 'bimber_archive_featured_entries_hide_elements', array(
	'label'           => esc_html__( 'Featured Entries Hide Elements', 'bimber' ),
	'section'         => 'bimber_posts_archive_section',
	'settings'        => $bimber_option_name . '[archive_featured_entries_hide_elements]',
	'choices'         => array(
		'shares'        => esc_html__( 'Shares', 'bimber' ),
		'views'         => esc_html__( 'Views', 'bimber' ),
		'comments_link' => esc_html__( 'Comments Link', 'bimber' ),
		'categories'    => esc_html__( 'Categories', 'bimber' ),
	),
	'active_callback' => 'bimber_customizer_archive_has_featured_entries',
) ) );

/**
 * Check whether featured entries are enabled for archive pages
 *
 * @param WP_Customize_Control $control     Control instance for which this callback is executed.
 *
 * @return bool
 */
function bimber_customizer_archive_has_featured_entries( $control ) {
	$type = $control->manager->get_setting( bimber_get_theme_id() . '[archive_featured_entries]' )->value();

	return 'none' !== $type;
}


// Title.
$wp_customize->add_setting( $bimber_option_name . '[archive_title]', array(
	'default'           => $bimber_customizer_defaults['archive_title'],
	'type'              => 'option',
	'capability'        => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_text_field',
) );

$wp_customize->add_control( 'bimber_archive_title', array(
	'label'           => esc_html__( 'Title', 'bimber' ),
	'section'         => 'bimber_posts_archive_section',
	'settings'        => $bimber_option_name . '[archive_title]',
	'type'            => 'text',
	'input_attrs'     => array(
		'placeholder' => esc_html__( 'Leave empty to use default', 'bimber' ),
	),
) );


// Hide title.
$wp_customize->add_setting( $bimber_option_name . '[archive_title_hide]', array(
	'default'           => $bimber_customizer_defaults['archive_title_hide'],
	'type'              => 'option',
	'capability'        => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_text_field',
) );

$wp_customize->add_control( 'bimber_archive_title_hide', array(
	'label'    => esc_html__( 'Hide title', 'bimber' ),
	'section'  => 'bimber_posts_archive_section',
	'settings' => $bimber_option_name . '[archive_title_hide]',
	'type'     => 'checkbox',
) );

// Posts Per Page.
$wp_customize->add_setting( $bimber_option_name . '[archive_posts_per_page]', array(
	'default'           => $bimber_customizer_defaults['archive_posts_per_page'],
	'type'              => 'option',
	'capability'        => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_text_field',
) );

$wp_customize->add_control( 'bimber_archive_posts_per_page', array(
	'label'    => esc_html__( 'Entries per page', 'bimber' ),
	'section'  => 'bimber_posts_archive_section',
	'settings' => $bimber_option_name . '[archive_posts_per_page]',
	'type'     => 'number',
) );


// Pagination.
$wp_customize->add_setting( $bimber_option_name . '[archive_pagination]', array(
	'default'           => $bimber_customizer_defaults['archive_pagination'],
	'type'              => 'option',
	'capability'        => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_text_field',
) );

$wp_customize->add_control( 'bimber_archive_pagination', array(
	'label'    => esc_html__( 'Pagination', 'bimber' ),
	'section'  => 'bimber_posts_archive_section',
	'settings' => $bimber_option_name . '[archive_pagination]',
	'type'     => 'select',
	'choices'  => array(
		'load-more'                 => esc_html__( 'Load More', 'bimber' ),
		'infinite-scroll'           => esc_html__( 'Infinite Scroll', 'bimber' ),
		'infinite-scroll-on-demand' => esc_html__( 'Infinite Scroll (first load via click)', 'bimber' ),
		'pages'                     => esc_html__( 'Prev/Next Pages', 'bimber' ),
	),
) );


// Hide Elements.
$wp_customize->add_setting( $bimber_option_name . '[archive_hide_elements]', array(
	'default'           => $bimber_customizer_defaults['archive_hide_elements'],
	'type'              => 'option',
	'capability'        => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_text_field',
) );

$wp_customize->add_control( new Bimber_Customize_Multi_Checkbox_Control( $wp_customize, 'bimber_archive_hide_elements', array(
	'label'    => esc_html__( 'Hide Elements', 'bimber' ),
	'section'  => 'bimber_posts_archive_section',
	'settings' => $bimber_option_name . '[archive_hide_elements]',
	'choices'  => array(
		'featured_media' => esc_html__( 'Featured Media', 'bimber' ),
		'categories'     => esc_html__( 'Categories', 'bimber' ),
		'title'          => esc_html__( 'Title', 'bimber' ),
		'summary'        => esc_html__( 'Summary', 'bimber' ),
		'author'         => esc_html__( 'Author', 'bimber' ),
		'avatar'         => esc_html__( 'Avatar', 'bimber' ),
		'date'           => esc_html__( 'Date', 'bimber' ),
		'shares'         => esc_html__( 'Shares', 'bimber' ),
		'views'          => esc_html__( 'Views', 'bimber' ),
		'comments_link'  => esc_html__( 'Comments Link', 'bimber' ),
	),
) ) );


// Newsletter.
$wp_customize->add_setting( $bimber_option_name . '[archive_newsletter]', array(
	'default'           => $bimber_customizer_defaults['archive_newsletter'],
	'type'              => 'option',
	'capability'        => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_text_field',
) );

$wp_customize->add_control( 'bimber_archive_newsletter', array(
	'label'    => esc_html__( 'Newsletter', 'bimber' ),
	'section'  => 'bimber_posts_archive_section',
	'settings' => $bimber_option_name . '[archive_newsletter]',
	'type'     => 'select',
	'choices'  => array(
		'standard' => esc_html__( 'inject into post collection', 'bimber' ),
		'none'     => esc_html__( 'hide', 'bimber' ),
	),
) );

$wp_customize->add_setting( $bimber_option_name . '[archive_newsletter_after_post]', array(
	'default'           => $bimber_customizer_defaults['archive_newsletter_after_post'],
	'type'              => 'option',
	'capability'        => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_text_field',
) );

$wp_customize->add_control( 'bimber_archive_newsletter_after_post', array(
	'label'           => esc_html__( 'Inject newsletter after post', 'bimber' ),
	'section'         => 'bimber_posts_archive_section',
	'settings'        => $bimber_option_name . '[archive_newsletter_after_post]',
	'type'            => 'number',
	'input_attrs'     => array(
		'placeholder' => esc_html__( 'eg. 2', 'bimber' ),
	),
	'active_callback' => 'bimber_customizer_is_archive_newsletter_checked',
) );

/**
 * Check whether newsletter is enabled for archive pages
 *
 * @param WP_Customize_Control $control     Control instance for which this callback is executed.
 *
 * @return bool
 */
function bimber_customizer_is_archive_newsletter_checked( $control ) {
	return $control->manager->get_setting( bimber_get_theme_id() . '[archive_newsletter]' )->value() === 'standard';
}


// Ad.
$wp_customize->add_setting( $bimber_option_name . '[archive_ad]', array(
	'default'           => $bimber_customizer_defaults['archive_ad'],
	'type'              => 'option',
	'capability'        => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_text_field',
) );

$wp_customize->add_control( 'bimber_archive_ad', array(
	'label'    => esc_html__( 'Ad', 'bimber' ),
	'section'  => 'bimber_posts_archive_section',
	'settings' => $bimber_option_name . '[archive_ad]',
	'type'     => 'select',
	'choices'  => array(
		'standard' => esc_html__( 'inject into post collection', 'bimber' ),
		'none'     => esc_html__( 'hide', 'bimber' ),
	),
) );

$wp_customize->add_setting( $bimber_option_name . '[archive_ad_after_post]', array(
	'default'           => $bimber_customizer_defaults['archive_ad_after_post'],
	'type'              => 'option',
	'capability'        => 'edit_theme_options',
	'sanitize_callback' => 'sanitize_text_field',
) );

$wp_customize->add_control( 'bimber_archive_ad_after_post', array(
	'label'           => esc_html__( 'Inject ad after post', 'bimber' ),
	'section'         => 'bimber_posts_archive_section',
	'settings'        => $bimber_option_name . '[archive_ad_after_post]',
	'type'            => 'number',
	'input_attrs'     => array(
		'placeholder' => esc_html__( 'eg. 5', 'bimber' ),
	),
	'active_callback' => 'bimber_customizer_is_archive_ad_checked',
) );

/**
 * Check whether ad is enabled for archive pages
 *
 * @param WP_Customize_Control $control     Control instance for which this callback is executed.
 *
 * @return bool
 */
function bimber_customizer_is_archive_ad_checked( $control ) {
	return $control->manager->get_setting( bimber_get_theme_id() . '[archive_ad]' )->value() === 'standard';
}
