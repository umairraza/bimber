<?php
/**
 * The Template for displaying ad on the left side of a stream template.
 *
 * @package Bimber_Theme
 * @license For the full license information, please view the Licensing folder
 * that was distributed with this source code.
 */

// Prevent direct script access.
if ( ! defined( 'ABSPATH' ) ) {
	die( 'No direct script access allowed' );
}
?>

<?php if ( bimber_can_use_plugin( 'quick-adsense-reloaded/quick-adsense-reloaded.php' ) ) : ?>

	<?php if ( quads_has_ad( 'bimber_left_stream' ) ) : ?>

		<div class="g1-advertisement g1-advertisement-left-stream">

			<?php quads_ad( array( 'location' => 'bimber_left_stream' ) ); ?>

		</div>

	<?php else : ?>

		<?php get_template_part( 'template-parts/ad-not-allowed' ); ?>

	<?php endif; ?>

<?php else : ?>

	<?php get_template_part( 'template-parts/ad-plugin-required' ); ?>

<?php endif; ?>
