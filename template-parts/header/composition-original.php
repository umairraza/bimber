
		<div class="g1-header g1-row g1-row-layout-page">
			<div class="g1-row-inner">
				<div class="g1-column">
					<?php get_template_part( 'template-parts/header/id' ); ?>
					<?php get_template_part( 'template-parts/nav-quick' ); ?>
				</div>
			</div>
			<div class="g1-row-background"></div>
		</div>

<?php if ( bimber_use_sticky_header() ) : ?>
	<div class="g1-sticky-top-wrapper">
<?php endif; ?>

		<div class="g1-row g1-row-layout-page g1-navbar">
			<div class="g1-row-inner">
				<div class="g1-column g1-dropable">
					<?php if ( has_nav_menu( 'bimber_primary_nav' ) ) : ?>
						<a class="g1-hamburger g1-hamburger-show" href="">
							<span class="g1-hamburger-icon"></span>
							<span class="g1-hamburger-label"><?php esc_html_e( 'Menu', 'bimber' ); ?></span>
						</a>
					<?php endif; ?>

					<?php $bimber_small_logo = bimber_get_small_logo(); ?>
					<?php if ( ! empty( $bimber_small_logo ) ) : ?>
						<a class="g1-logo-small-wrapper" href="<?php echo esc_url( home_url( '/' ) ); ?>">
							<?php
							printf(
								'<img class="g1-logo-small" width="%d" height="%d" src="%s" %s alt="%s" />',
								absint( $bimber_small_logo['width'] ),
								absint( $bimber_small_logo['height'] ),
								esc_url( $bimber_small_logo['src'] ),
								isset( $bimber_small_logo['srcset'] ) ? sprintf( 'srcset="%s"', esc_attr( $bimber_small_logo['srcset'] ) ) : '',
								""
							);
							?>
						</a>
					<?php endif; ?>

					<!-- BEGIN .g1-primary-nav -->
					<?php
					if ( has_nav_menu( 'bimber_primary_nav' ) ) :
						wp_nav_menu( array(
							'theme_location'  => 'bimber_primary_nav',
							'container'       => 'nav',
							'container_class' => 'g1-primary-nav',
							'container_id'    => 'g1-primary-nav',
							'menu_class'      => 'g1-primary-nav-menu',
							'menu_id'         => 'g1-primary-nav-menu',
							'depth'           => 0,
						) );
					endif;
					?>
					<!-- END .g1-primary-nav -->

					<?php get_template_part( 'template-parts/nav-user' ); ?>
					<?php get_template_part( 'template-parts/header/drop-search' ); ?>
					<?php get_template_part( 'template-parts/header/drop-socials' ); ?>
				</div><!-- .g1-column -->

			</div>
		</div>
<?php if ( bimber_use_sticky_header() ) : ?>
	</div>
<?php endif;
