<?php
/**
 * The template part for displaying the featured content.
 *
 * @license For the full license information, please view the Licensing folder
 * that was distributed with this source code.
 *
 * @package Bimber_Theme
 */

// Prevent direct script access.
if ( ! defined( 'ABSPATH' ) ) {
	die( 'No direct script access allowed' );
}

$bimber_template_data                      = bimber_get_template_part_data();
$bimber_featured_entries                   = $bimber_template_data['featured_entries'];
$bimber_featured_entries['posts_per_page'] = 1;

$bimber_featured_ids = bimber_get_featured_posts_ids( $bimber_featured_entries );

$bimber_query_args = array();

if ( ! empty( $bimber_featured_ids ) ) {
	$bimber_query_args['post__in']            = $bimber_featured_ids;
	$bimber_query_args['orderby']             = 'post__in';
	$bimber_query_args['ignore_sticky_posts'] = true;
}

$bimber_query = new WP_Query( $bimber_query_args );

$bimber_class = array(
	'g1-delta',
	'g1-delta-2nd',
	'archive-featured-title',
);

if ( $bimber_template_data['featured_entries_title_hide'] ) {
	$bimber_class[] = 'screen-reader-text';
}
?>

<?php if ( $bimber_query->have_posts() ) : ?>
	<section class="archive-featured">
		<?php bimber_set_template_part_data( $bimber_featured_entries ); ?>

		<h2 class="<?php echo implode( ' ', array_map( 'sanitize_html_class', $bimber_class ) ); ?>"><strong><?php echo wp_kses_post( $bimber_template_data['featured_entries_title'] ); ?></strong></h2>
		<div class="g1-mosaic">
			<?php while ( $bimber_query->have_posts() ) : $bimber_query->the_post(); ?>

				<?php get_template_part( 'template-parts/content-tile-xl', get_post_format() ); ?>

			<?php endwhile;

			bimber_reset_template_part_data();
			wp_reset_postdata();
			?>
		</div>
	</section>
<?php endif;
