<?php
/**
 *  The template for displaying featured entries
 *
 * @license For the full license information, please view the Licensing folder
 * that was distributed with this source code.
 *
 * @package Bimber_Theme
 */

// Prevent direct script access.
if ( ! defined( 'ABSPATH' ) ) {
	die( 'No direct script access allowed' );
}

$bimber_type = bimber_get_theme_option( 'featured_entries', 'type' );

if ( 'none' === $bimber_type ) {
	return;
}

$bimber_title = esc_html__( 'Latest stories', 'bimber' );

switch ( $bimber_type ) {
	case 'most_viewed':
		$bimber_title = esc_html__( 'Most viewed stories', 'bimber' );
		break;

	case 'most_shared':
		$bimber_title = esc_html__( 'Most shared stories', 'bimber' );
		break;
}
?>

<aside class="g1-row g1-row-layout-page g1-featured-row">
	<div class="g1-row-inner">
		<div class="g1-column">

			<h2 class="g1-zeta g1-zeta-2nd g1-featured-title"><?php echo wp_kses_post( $bimber_title ); ?></h2>

			<?php
			$bimber_query = bimber_get_global_featured_entries_query();

			if ( $bimber_query->have_posts() ) {
				$bimber_template = bimber_get_theme_option( 'featured_entries', 'template' );
				$bimber_template = 'template-parts/content-' . $bimber_template . '-xs';

				$bimber_settings = apply_filters( 'bimber_global_featured_entry_settings', array(
					'elements' => array(
						'featured_media' => true,
						'avatar'         => false,
						'categories'     => false,
						'title'          => true,
						'summary'        => false,
						'author'         => false,
						'date'           => false,
						'shares'         => false,
						'views'          => false,
						'comments_link'  => false,
					),
				) );

				bimber_set_template_part_data( $bimber_settings );

				?>
				<div class="g1-featured g1-featured-viewport-start">
					<a href="#"
					   class="g1-featured-arrow g1-featured-arrow-prev"><?php esc_html_e( 'Previous', 'bimber' ) ?></a>
					<a href="#"
					   class="g1-featured-arrow g1-featured-arrow-next"><?php esc_html_e( 'Next', 'bimber' ) ?></a>

					<ul class="g1-featured-items">
						<?php while ( $bimber_query->have_posts() ) : $bimber_query->the_post(); ?>

							<li class="g1-featured-item">
								<?php get_template_part( $bimber_template, get_post_format() ); ?>
							</li>

						<?php endwhile; ?>
					</ul>

					<div class="g1-featured-fade g1-featured-fade-before"></div>
					<div class="g1-featured-fade g1-featured-fade-after"></div>
				</div>
				<?php

				bimber_reset_template_part_data();
				wp_reset_postdata();
			} else {
				?>
				<div class="g1-featured-no-results">
					<p>
						<?php esc_html_e( 'No featured entries match the criteria.', 'bimber' ); ?><br/>
						<?php esc_html_e( 'For more information please refer to the documentation.', 'bimber' ); ?>
					</p>
				</div>
				<?php
			}
			?>
		</div>
	</div>
	<div class="g1-row-background"></div>
</aside>
