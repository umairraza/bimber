<?php
/**
 * The template part for displaying content
 *
 * @package Bimber_Theme
 */

?>
<?php

$bimber_entry_data = bimber_get_template_part_data();
$bimber_elements   = $bimber_entry_data['elements'];
?>

<article <?php post_class( 'entry-tpl-listxs' ); ?>>
	<?php
	if ( $bimber_elements['featured_media'] ) :
		bimber_render_entry_featured_media( array(
			'size' => 'bimber-list-xs',
		) );
	endif;
	?>

	<header class="entry-header">
		<?php the_title( sprintf( '<h3 class="g1-epsilon g1-epsilon-1st entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h3>' ); ?>
	</header>
</article>
