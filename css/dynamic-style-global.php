<?php
/**
 * Global styles
 *
 * @license For the full license information, please view the Licensing folder
 * that was distributed with this source code.
 *
 * @package Bimber_Theme
 */

$bimber_filter_hex = array( 'options' => array( 'regexp' => '/^([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$/' ) );

$bimber_stack = bimber_get_theme_option( 'global', 'stack' );

$bimber_page_layout = bimber_get_theme_option( 'global', 'layout' );

$bimber_body_background          = array();
$bimber_body_background['color'] = new Bimber_Color( bimber_get_theme_option( 'global', 'background_color' ) );

$bimber_cs_1_background = new Bimber_Color( bimber_get_theme_option( 'content', 'cs_1_background_color' ) );
$bimber_cs_1_background_variations = bimber_get_color_variations( $bimber_cs_1_background );
$bimber_cs_1_background_5          = new Bimber_Color( $bimber_cs_1_background_variations['tone_5_90_hex'] );
$bimber_cs_1_background_10         = new Bimber_Color( $bimber_cs_1_background_variations['tone_20_20_hex'] );

$bimber_cs_1_text1                  = new Bimber_Color( bimber_get_theme_option( 'content', 'cs_1_text1' ) );
$bimber_cs_1_text2                  = new Bimber_Color( bimber_get_theme_option( 'content', 'cs_1_text2' ) );
$bimber_cs_1_text3                  = new Bimber_Color( bimber_get_theme_option( 'content', 'cs_1_text3' ) );
$bimber_cs_1_accent1                = new Bimber_Color( bimber_get_theme_option( 'content', 'cs_1_accent1' ) );
$bimber_cs_2_background             = new Bimber_Color( bimber_get_theme_option( 'content', 'cs_2_background_color' ) );
$bimber_cs_2_text1                  = new Bimber_Color( bimber_get_theme_option( 'content', 'cs_2_text1' ) );

$bimber_cs_2_background2 = $bimber_cs_2_background;
if ( strlen( bimber_get_theme_option( 'content', 'cs_2_background2_color' ) ) ) {
	$bimber_cs_2_background2 = new Bimber_Color( bimber_get_theme_option( 'content', 'cs_2_background2_color' ) );
}


$bimber_trending_background         = new Bimber_Color( bimber_get_theme_option( 'trending', 'background_color' ) );
$bimber_hot_background              = new Bimber_Color( bimber_get_theme_option( 'hot', 'background_color' ) );
$bimber_popular_background          = new Bimber_Color( bimber_get_theme_option( 'popular', 'background_color' ) );
?>
body.g1-layout-boxed {
background-color: #<?php echo filter_var( $bimber_body_background['color']->get_hex(), FILTER_VALIDATE_REGEXP, $bimber_filter_hex ); ?>;
}

.g1-layout-boxed .g1-row-layout-page {
max-width: 1212px;
}

/* Global Color Scheme */
.g1-row-layout-page > .g1-row-background,
.g1-sharebar > .g1-row-background,
.g1-current-background {
background-color: #<?php echo filter_var( $bimber_cs_1_background->get_hex(), FILTER_VALIDATE_REGEXP, $bimber_filter_hex ); ?>;
}

input,
select,
textarea {
border-color: #<?php echo filter_var( $bimber_cs_1_background_10->get_hex(), FILTER_VALIDATE_REGEXP, $bimber_filter_hex ); ?>;
}


a,
.entry-title > a:hover,
.entry-meta a:hover,
.menu-item > a:hover,
.current-menu-item > a,
.mtm-drop-expanded > a,
.g1-link-right:after,
.g1-link-left:before,
.g1-nav-single-prev > a > span:before,
.g1-nav-single-next > a > span:after,
.g1-nav-single-prev > a:hover > strong,
.g1-nav-single-prev > a:hover > span,
.g1-nav-single-next > a:hover > strong,
.g1-nav-single-next > a:hover > span,
#primary .mashsb-main .mashsb-count {
color: #<?php echo filter_var( $bimber_cs_1_accent1->get_hex(), FILTER_VALIDATE_REGEXP, $bimber_filter_hex ); ?>;
}

input[type="submit"],
input[type="reset"],
input[type="button"],
button,
.g1-button-solid,
.g1-button-solid:hover,
.g1-arrow-solid,
.entry-categories .entry-category:hover,
.author-link,
.author-info .author-link,
.g1-box-icon,
.snax .snax-voting-simple .snax-voting-upvote:hover,
.snax .snax-voting-simple .snax-voting-downvote:hover,
.wyr-reaction:hover .wyr-reaction-button,
.wyr-reaction-voted .wyr-reaction-button,
.wyr-reaction .wyr-reaction-bar {
border-color: #<?php echo filter_var( $bimber_cs_2_background->get_hex(), FILTER_VALIDATE_REGEXP, $bimber_filter_hex ); ?>;
background-color: #<?php echo filter_var( $bimber_cs_2_background->get_hex(), FILTER_VALIDATE_REGEXP, $bimber_filter_hex ); ?>;
color: #<?php echo filter_var( $bimber_cs_2_text1->get_hex(), FILTER_VALIDATE_REGEXP, $bimber_filter_hex ); ?>;
}

.entry-counter:before {
border-color: #<?php echo filter_var( $bimber_cs_2_background->get_hex(), FILTER_VALIDATE_REGEXP, $bimber_filter_hex ); ?>;
background-color: #<?php echo filter_var( $bimber_cs_2_background->get_hex(), FILTER_VALIDATE_REGEXP, $bimber_filter_hex ); ?>;
color: #<?php echo filter_var( $bimber_cs_2_background->get_hex(), FILTER_VALIDATE_REGEXP, $bimber_filter_hex ); ?>;
}

.entry-counter:after {
color: #<?php echo filter_var( $bimber_cs_2_text1->get_hex(), FILTER_VALIDATE_REGEXP, $bimber_filter_hex ); ?>;
}



.g1-button-simple,
input.g1-button-simple,
button.g1-button-simple {
	border-color: currentColor;
	background-color: transparent;
	color: #<?php echo filter_var( $bimber_cs_2_background->get_hex(), FILTER_VALIDATE_REGEXP, $bimber_filter_hex ); ?>;
}



.g1-drop-toggle-arrow {
color: #<?php echo filter_var( $bimber_cs_1_background->get_hex(), FILTER_VALIDATE_REGEXP, $bimber_filter_hex ); ?>;
}


.g1-quick-nav-tabs .menu-item-type-g1-trending > a,
.entry-flag-trending {
border-color: #<?php echo filter_var( $bimber_trending_background->get_hex(), FILTER_VALIDATE_REGEXP, $bimber_filter_hex ); ?>;
background-color: #<?php echo filter_var( $bimber_trending_background->get_hex(), FILTER_VALIDATE_REGEXP, $bimber_filter_hex ); ?>;
color: #fff;
}

.g1-quick-nav-tabs .menu-item-type-g1-hot > a,
.entry-flag-hot {
border-color: #<?php echo filter_var( $bimber_hot_background->get_hex(), FILTER_VALIDATE_REGEXP, $bimber_filter_hex ); ?>;
background-color: #<?php echo filter_var( $bimber_hot_background->get_hex(), FILTER_VALIDATE_REGEXP, $bimber_filter_hex ); ?>;
color: #fff;
}

.g1-quick-nav-tabs .menu-item-type-g1-popular > a,
.entry-flag-popular {
border-color: #<?php echo filter_var( $bimber_popular_background->get_hex(), FILTER_VALIDATE_REGEXP, $bimber_filter_hex ); ?>;
background-color: #<?php echo filter_var( $bimber_popular_background->get_hex(), FILTER_VALIDATE_REGEXP, $bimber_filter_hex ); ?>;
color: #fff;
}


<?php if ( 'miami' === $bimber_stack && $bimber_cs_2_background->get_hex() !== $bimber_cs_2_background2->get_hex() ) : ?>
.g1-box {
	background-image:    -owg-linear-gradient(to bottom right, #<?php echo filter_var( $bimber_cs_2_background->get_hex(), FILTER_VALIDATE_REGEXP, $bimber_filter_hex ); ?>, #<?php echo filter_var( $bimber_cs_2_background2->get_hex(), FILTER_VALIDATE_REGEXP, $bimber_filter_hex ); ?>);
	background-image: -webkit-linear-gradient(to bottom right, #<?php echo filter_var( $bimber_cs_2_background->get_hex(), FILTER_VALIDATE_REGEXP, $bimber_filter_hex ); ?>, #<?php echo filter_var( $bimber_cs_2_background2->get_hex(), FILTER_VALIDATE_REGEXP, $bimber_filter_hex ); ?>);
	background-image:    -moz-linear-gradient(to bottom right, #<?php echo filter_var( $bimber_cs_2_background->get_hex(), FILTER_VALIDATE_REGEXP, $bimber_filter_hex ); ?>, #<?php echo filter_var( $bimber_cs_2_background2->get_hex(), FILTER_VALIDATE_REGEXP, $bimber_filter_hex ); ?>);
	background-image:      -o-linear-gradient(to bottom right, #<?php echo filter_var( $bimber_cs_2_background->get_hex(), FILTER_VALIDATE_REGEXP, $bimber_filter_hex ); ?>, #<?php echo filter_var( $bimber_cs_2_background2->get_hex(), FILTER_VALIDATE_REGEXP, $bimber_filter_hex ); ?>);
	background-image:         linear-gradient(to bottom right, #<?php echo filter_var( $bimber_cs_2_background->get_hex(), FILTER_VALIDATE_REGEXP, $bimber_filter_hex ); ?>, #<?php echo filter_var( $bimber_cs_2_background2->get_hex(), FILTER_VALIDATE_REGEXP, $bimber_filter_hex ); ?>);
}
<?php endif;
